﻿using knowit_skatt_api.Application.Features.Trips.Commands;
using knowit_skatt_api.Application.Features.Trips.Commands.CreateTrip;
//using knowit_skatt_api.Application.Features.Trips.Commands.DeleteTripById;
//using knowit_skatt_api.Application.Features.Trips.Commands.UpdateTrip;
using knowit_skatt_api.Application.Features.Trips.Queries.GetAllTrips;
using knowit_skatt_api.Application.Features.Trips.Queries.GetTripById;
using knowit_skatt_api.Application.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace knowit_skatt_api.WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class TripController : BaseApiController
    {
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetAllTripsParameter filter)
        {

            return Ok(await Mediator.Send(new GetAllTripsQuery() { PageSize = filter.PageSize, PageNumber = filter.PageNumber }));
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetTripByIdQuery { Id = id }));
        }

        // POST api/<controller>
        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> Post(CreateTripCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        //// PUT api/<controller>/5
        //[HttpPut("{id}")]
        ////[Authorize]
        //public async Task<IActionResult> Put(int id, UpdateTripCommand command)
        //{
        //    if (id != command.Id)
        //    {
        //        return BadRequest();
        //    }
        //    return Ok(await Mediator.Send(command));
        //}

        //// DELETE api/<controller>/5
        //[HttpDelete("{id}")]
        ////[Authorize]
        //public async Task<IActionResult> Delete(int id)
        //{
        //    return Ok(await Mediator.Send(new DeleteTripByIdCommand { Id = id }));
        //}
    }
}
