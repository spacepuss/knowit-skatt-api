﻿using knowit_skatt_api.Application.Features.Fees.Queries.GetFeeByRegNr;
//using knowit_skatt_api.Application.Features.Fees.Queries.GetFeeByRegNr;
using knowit_skatt_api.Application.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace knowit_skatt_api.WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class FeeController : BaseApiController
    {
        // GET api/<controller>/5
        [HttpGet("{regNr}")]
        public async Task<IActionResult> Get(string regNr)
        {
            return Ok(await Mediator.Send(new GetFeeByRegNrQuery { RegNr = regNr }));
        }
    }
}
