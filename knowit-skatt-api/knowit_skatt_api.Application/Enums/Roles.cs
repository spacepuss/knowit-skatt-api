﻿using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Application.Enums
{
    public enum Roles
    {
        SuperAdmin,
        Admin,
        Moderator,
        Basic
    }
}
