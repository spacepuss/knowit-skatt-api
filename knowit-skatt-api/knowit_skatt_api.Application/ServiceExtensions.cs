﻿using AutoMapper;
using FluentValidation;
using knowit_skatt_api.Application.Behaviours;
using knowit_skatt_api.Application.Features.Products.Commands.CreateProduct;
using knowit_skatt_api.Application.Features.Trips.Services;
using knowit_skatt_api.Application.Interfaces;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace knowit_skatt_api.Application
{
    public static class ServiceExtensions
    {
        public static void AddApplicationLayer(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            services.AddTransient<IExemptedDateService, ExemptedDateService>();

        }
    }
}
