﻿using AutoMapper;
using knowit_skatt_api.Application.Features.Products.Commands.CreateProduct;
using knowit_skatt_api.Application.Features.Products.Queries.GetAllProducts;
using knowit_skatt_api.Application.Features.Vehicles.Commands.CreateVehicle;
using knowit_skatt_api.Application.Features.Vehicles.Queries.GetAllVehicles;
using knowit_skatt_api.Application.Features.Trips.Commands.CreateTrip;
using knowit_skatt_api.Application.Features.Trips.Queries.GetAllTrips;
using knowit_skatt_api.Application.Features.Fees.Queries.GetFeeByRegNr;
using knowit_skatt_api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<Product, GetAllProductsViewModel>().ReverseMap();
            CreateMap<CreateProductCommand, Product>();
            CreateMap<GetAllProductsQuery, GetAllProductsParameter>();
            
            CreateMap<Vehicle, GetAllVehiclesViewModel>().ReverseMap();
            CreateMap<CreateVehicleCommand, Vehicle>();
            CreateMap<GetAllVehiclesQuery, GetAllVehiclesParameter>();
            
            CreateMap<Trip, GetAllTripsViewModel>().ReverseMap();
            CreateMap<CreateTripCommand, Trip>();
            CreateMap<GetAllTripsQuery, GetAllTripsParameter>();

            CreateMap<Fee, GetFeeByRegNrViewModel>().ReverseMap();
            //CreateMap<CreateFeeCommand, Fee>();
            //CreateMap<GetAllFeesQuery, GetAllFeesParameter>();
        }
    }
}
