﻿using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Application.Interfaces
{
    public interface IDateTimeService
    {
        DateTime NowUtc { get; }
    }
}
