﻿using knowit_skatt_api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Interfaces.Repositories
{
    public interface ITripRepositoryAsync : IGenericRepositoryAsync<Trip>
    {
        Task<bool> IsUniqueDateAsync(DateTime date);
        Task<Trip> AddTripAsync(Trip entity);
    }
}
