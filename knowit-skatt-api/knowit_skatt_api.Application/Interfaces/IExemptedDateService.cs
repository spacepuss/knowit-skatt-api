﻿using knowit_skatt_api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Interfaces
{
    public interface IExemptedDateService
    {
        List<ExemptedDate> GetExemptedDatesAsync();
    }
}
