﻿using AutoMapper;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Application.Wrappers;
using knowit_skatt_api.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Features.Trips.Commands.CreateTrip
{
    public partial class CreateTripCommand : IRequest<Response<int>>
    {
        public DateTime Date { get; set; }
        public decimal Fee { get; set; }
        public string Type { get; set; }
        public string RegNr { get; set; }
    }
    public class CreateTripCommandHandler : IRequestHandler<CreateTripCommand, Response<int>>
    {
        private readonly ITripRepositoryAsync _tripRepository;
        private readonly IMapper _mapper;
        public CreateTripCommandHandler(ITripRepositoryAsync tripRepository, IMapper mapper)
        {
            _tripRepository = tripRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateTripCommand request, CancellationToken cancellationToken)
        {
            var trip = _mapper.Map<Trip>(request);
            //await _tripRepository.AddAsync(trip);
            await _tripRepository.AddTripAsync(trip);
            return new Response<int>(trip.Id);
        }
    }
}
