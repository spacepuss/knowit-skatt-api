﻿using FluentValidation;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Domain.Entities;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Features.Trips.Commands.CreateTrip
{
    public class CreateTripCommandValidator : AbstractValidator<CreateTripCommand>
    {
        private readonly ITripRepositoryAsync tripRepository;

        public CreateTripCommandValidator(ITripRepositoryAsync tripRepository)
        {
            this.tripRepository = tripRepository;

            RuleFor(t => t.Date)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(t => t.Fee)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(t => t.Type)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull();

            RuleFor(t => t.RegNr)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(6).WithMessage("{PropertyName} must not exceed 6 characters.");
        }

        private async Task<bool> IsUniqueDate(DateTime date, CancellationToken cancellationToken)
        {
            return await tripRepository.IsUniqueDateAsync(date);
        }
    }
}
