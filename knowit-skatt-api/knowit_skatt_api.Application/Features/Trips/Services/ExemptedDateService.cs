﻿using knowit_skatt_api.Application.Interfaces;
using knowit_skatt_api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace knowit_skatt_api.Application.Features.Trips.Services
{
    public class ExemptedDateService : IExemptedDateService
    {
        // Todo: Store these dates in a data store instead of hard-coding them in code.
        public List<ExemptedDate> _exemptedDates { get; }

        public ExemptedDateService()
        {
            _exemptedDates = new List<ExemptedDate>();

            // todo: Add real dates
            _exemptedDates.Add(new ExemptedDate { Date = DateTime.Now });
            _exemptedDates.Add(new ExemptedDate { Date = DateTime.Now.AddDays(1) });
        }

        public List<ExemptedDate> GetExemptedDatesAsync()
        {
            return _exemptedDates;
        }
    }
}
