﻿using AutoMapper;
using knowit_skatt_api.Application.Filters;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Features.Trips.Queries.GetAllTrips
{
    public class GetAllTripsQuery : IRequest<PagedResponse<IEnumerable<GetAllTripsViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllTripsQueryHandler : IRequestHandler<GetAllTripsQuery, PagedResponse<IEnumerable<GetAllTripsViewModel>>>
    {
        private readonly ITripRepositoryAsync _tripRepository;
        private readonly IMapper _mapper;
        public GetAllTripsQueryHandler(ITripRepositoryAsync tripRepository, IMapper mapper)
        {
            _tripRepository = tripRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<GetAllTripsViewModel>>> Handle(GetAllTripsQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllTripsParameter>(request);
            var trip = await _tripRepository.GetPagedReponseAsync(validFilter.PageNumber, validFilter.PageSize);
            var tripViewModel = _mapper.Map<IEnumerable<GetAllTripsViewModel>>(trip);
            return new PagedResponse<IEnumerable<GetAllTripsViewModel>>(tripViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
