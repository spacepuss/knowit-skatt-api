﻿using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Application.Features.Trips.Queries.GetAllTrips
{
    public class GetAllTripsViewModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Fee { get; set; }
    }
}
