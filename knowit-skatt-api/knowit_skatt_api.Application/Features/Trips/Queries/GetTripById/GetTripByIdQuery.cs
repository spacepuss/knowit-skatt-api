﻿using knowit_skatt_api.Application.Exceptions;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Application.Wrappers;
using knowit_skatt_api.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Features.Trips.Queries.GetTripById
{
    public class GetTripByIdQuery : IRequest<Response<Trip>>
    {
        public int Id { get; set; }
        public class GetTripByIdQueryHandler : IRequestHandler<GetTripByIdQuery, Response<Trip>>
        {
            private readonly ITripRepositoryAsync _tripRepository;
            public GetTripByIdQueryHandler(ITripRepositoryAsync tripRepository)
            {
                _tripRepository = tripRepository;
            }
            public async Task<Response<Trip>> Handle(GetTripByIdQuery query, CancellationToken cancellationToken)
            {
                var trip = await _tripRepository.GetByIdAsync(query.Id);
                if (trip == null) throw new ApiException($"Trip Not Found.");
                return new Response<Trip>(trip);
            }
        }
    }
}
