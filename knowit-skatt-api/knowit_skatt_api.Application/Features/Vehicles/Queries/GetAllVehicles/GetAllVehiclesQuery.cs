﻿using AutoMapper;
using knowit_skatt_api.Application.Filters;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Application.Wrappers;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Features.Vehicles.Queries.GetAllVehicles
{
    public class GetAllVehiclesQuery : IRequest<PagedResponse<IEnumerable<GetAllVehiclesViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllVehiclesQueryHandler : IRequestHandler<GetAllVehiclesQuery, PagedResponse<IEnumerable<GetAllVehiclesViewModel>>>
    {
        private readonly IVehicleRepositoryAsync _vehicleRepository;
        private readonly IMapper _mapper;
        public GetAllVehiclesQueryHandler(IVehicleRepositoryAsync vehicleRepository, IMapper mapper)
        {
            _vehicleRepository = vehicleRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<GetAllVehiclesViewModel>>> Handle(GetAllVehiclesQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllVehiclesParameter>(request);
            var vehicle = await _vehicleRepository.GetPagedReponseAsync(validFilter.PageNumber, validFilter.PageSize);
            var vehicleViewModel = _mapper.Map<IEnumerable<GetAllVehiclesViewModel>>(vehicle);
            return new PagedResponse<IEnumerable<GetAllVehiclesViewModel>>(vehicleViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
