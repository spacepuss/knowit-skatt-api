﻿using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Application.Features.Vehicles.Queries.GetAllVehicles
{
    public class GetAllVehiclesViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string RegNr { get; set; }
    }
}
