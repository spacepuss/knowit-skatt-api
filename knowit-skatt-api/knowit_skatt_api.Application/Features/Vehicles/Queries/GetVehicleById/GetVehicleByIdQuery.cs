﻿using knowit_skatt_api.Application.Exceptions;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Application.Wrappers;
using knowit_skatt_api.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Features.Vehicles.Queries.GetVehicleById
{
    public class GetVehicleByIdQuery : IRequest<Response<Vehicle>>
    {
        public int Id { get; set; }
        public class GetVehicleByIdQueryHandler : IRequestHandler<GetVehicleByIdQuery, Response<Vehicle>>
        {
            private readonly IVehicleRepositoryAsync _vehicleRepository;
            public GetVehicleByIdQueryHandler(IVehicleRepositoryAsync vehicleRepository)
            {
                _vehicleRepository = vehicleRepository;
            }
            public async Task<Response<Vehicle>> Handle(GetVehicleByIdQuery query, CancellationToken cancellationToken)
            {
                var vehicle = await _vehicleRepository.GetByIdAsync(query.Id);
                if (vehicle == null) throw new ApiException($"Vehicle Not Found.");
                return new Response<Vehicle>(vehicle);
            }
        }
    }
}
