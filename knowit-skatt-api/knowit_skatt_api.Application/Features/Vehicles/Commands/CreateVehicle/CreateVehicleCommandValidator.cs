﻿using FluentValidation;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Domain.Entities;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Features.Vehicles.Commands.CreateVehicle
{
    public class CreateVehicleCommandValidator : AbstractValidator<CreateVehicleCommand>
    {
        private readonly IVehicleRepositoryAsync vehicleRepository;

        public CreateVehicleCommandValidator(IVehicleRepositoryAsync vehicleRepository)
        {
            this.vehicleRepository = vehicleRepository;

            RuleFor(v => v.RegNr)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MustAsync(IsUniqueRegNr).WithMessage("{PropertyName} already exists.");
        }

        private async Task<bool> IsUniqueRegNr(string regNr, CancellationToken cancellationToken)
        {
            return await vehicleRepository.IsUniqueRegNrAsync(regNr);
        }
    }
}
