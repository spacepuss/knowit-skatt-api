﻿using AutoMapper;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Application.Wrappers;
using knowit_skatt_api.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Application.Features.Vehicles.Commands.CreateVehicle
{
    public partial class CreateVehicleCommand : IRequest<Response<int>>
    {
        public string Type { get; set; }
        public string RegNr { get; set; }
    }
    public class CreateVehicleCommandHandler : IRequestHandler<CreateVehicleCommand, Response<int>>
    {
        private readonly IVehicleRepositoryAsync _vehicleRepository;
        private readonly IMapper _mapper;
        public CreateVehicleCommandHandler(IVehicleRepositoryAsync vehicleRepository, IMapper mapper)
        {
            _vehicleRepository = vehicleRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateVehicleCommand request, CancellationToken cancellationToken)
        {
            var vehicle = _mapper.Map<Vehicle>(request);
            await _vehicleRepository.AddAsync(vehicle);
            return new Response<int>(vehicle.Id);
        }
    }
}
