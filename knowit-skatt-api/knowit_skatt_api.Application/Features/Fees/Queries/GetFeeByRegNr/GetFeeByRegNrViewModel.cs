﻿using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Application.Features.Fees.Queries.GetFeeByRegNr
{
    public class GetFeeByRegNrViewModel
    {
        public decimal MonthlyFee { get; set; }
    }
}
