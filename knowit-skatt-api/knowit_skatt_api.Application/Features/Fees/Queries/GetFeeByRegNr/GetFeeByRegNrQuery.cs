﻿using AutoMapper;
using knowit_skatt_api.Application.Exceptions;
using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Application.Wrappers;
using knowit_skatt_api.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using knowit_skatt_api.Application.Enums;
using knowit_skatt_api.Application.Interfaces;

namespace knowit_skatt_api.Application.Features.Fees.Queries.GetFeeByRegNr
{
    public class GetFeeByRegNrQuery : IRequest<Response<GetFeeByRegNrViewModel>>
    {
        public string RegNr { get; set; }
        public class GetFeeByRegNrQueryHandler : IRequestHandler<GetFeeByRegNrQuery, Response<GetFeeByRegNrViewModel>>
        {
            private readonly IFeeRepositoryAsync _feeRepository;
            private readonly IMapper _mapper;
            private readonly IExemptedDateService _exemptedDateService;

            public GetFeeByRegNrQueryHandler(IFeeRepositoryAsync feeRepository, IMapper mapper, IExemptedDateService exemptedDateService)
            {
                _feeRepository = feeRepository;
                _mapper = mapper;
                _exemptedDateService = exemptedDateService;
            }
            public async Task<Response<GetFeeByRegNrViewModel>> Handle(GetFeeByRegNrQuery query, CancellationToken cancellationToken)
            {
                var trips = await _feeRepository.GetByRegNrAsync(query.RegNr);
                if (trips == null) throw new ApiException($"No trips found.");

                if (IsRegNrExemptLookup(query.RegNr))
                    return new Response<GetFeeByRegNrViewModel>(new GetFeeByRegNrViewModel { MonthlyFee = 0 });

                var nonExemptedVehicles = WeedOutExemptedVehicles(trips);
                var regularDayTrips = WeedOutHolidays(nonExemptedVehicles);
                //var fullyFilteredTrips = WeedOutRedundantTripsPerHour(regularDayTrips);
                var monthlyFee = CalculateMonthlyFee(regularDayTrips);
                
                var feeViewModel = _mapper.Map<GetFeeByRegNrViewModel>(new GetFeeByRegNrViewModel { MonthlyFee = monthlyFee });
                return new Response<GetFeeByRegNrViewModel>(feeViewModel);
            }

            private bool IsRegNrExemptLookup(string regNr)
            {
                // Todo: Check if the vehicle is exempted or not
                return false;
            }

            private IReadOnlyList<TripProjection> WeedOutExemptedVehicles(IReadOnlyList<TripProjection> trips)
            {
                var nonExemptedVehicles = trips.Where(t => t.Type == ((int)VehicleTypes.StandardVehicle).ToString()).ToList();
                return nonExemptedVehicles;
            }

            private IReadOnlyList<TripProjection> WeedOutHolidays(IReadOnlyList<TripProjection> trips)
            {
                var exemptedDates = _exemptedDateService.GetExemptedDatesAsync();
                var nonExemptedDates = trips.Where(t => !exemptedDates.Any(e => e.Date.ToShortDateString() == t.Date.ToShortDateString())).ToList();

                var regularDayTrips = nonExemptedDates
                    .Where(n => !((int)n.Date.DayOfWeek == (int)DayOfWeek.Saturday))
                    .Where(n => !((int)n.Date.DayOfWeek == (int)DayOfWeek.Sunday))
                    .ToList();

                return regularDayTrips;
            }

            //private IReadOnlyList<TripProjection> WeedOutRedundantTripsPerHour(IReadOnlyList<TripProjection> trips)
            //{
            //    // todo
            //}

            private decimal CalculateMonthlyFee(IReadOnlyList<TripProjection> trips)
            {
                return trips.Sum(t => t.Fee);
            }
        }
    }
}
