﻿using knowit_skatt_api.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Domain.Entities
{
    public class VehicleTrip
    {
        public int VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }
        public int TripId { get; set; }
        public Trip Trip { get; set; }
    }
}
