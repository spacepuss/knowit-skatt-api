﻿using knowit_skatt_api.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Domain.Entities
{
    public class VehicleFee
    {
        public int VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }
        public int FeeId { get; set; }
        public Fee Fee { get; set; }
    }
}
