﻿using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Domain.Entities
{
    public class ExemptedDate
    {
        public DateTime Date { get; set; }
    }
}
