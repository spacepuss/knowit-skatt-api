﻿using knowit_skatt_api.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Domain.Entities
{
    public class Vehicle : AuditableBaseEntity
    {
        public string Type { get; set; }
        public string RegNr { get; set; }

        public ICollection<VehicleTrip> VehicleTrips { get; set; }
        //public ICollection<VehicleFee> VehicleFees { get; set; }
    }
}
