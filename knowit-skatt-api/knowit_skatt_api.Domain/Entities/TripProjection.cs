﻿using knowit_skatt_api.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace knowit_skatt_api.Domain.Entities
{
    public class TripProjection
    {
        public DateTime Date { get; set; }
        public decimal Fee { get; set; }
        public string Type { get; set; }
        public string RegNr { get; set; }
    }
}
