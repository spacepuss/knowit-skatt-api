﻿using knowit_skatt_api.Domain.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Domain.Entities
{
    public class Fee
    {
        public decimal MonthlyFee { get; set; }
    }
}
