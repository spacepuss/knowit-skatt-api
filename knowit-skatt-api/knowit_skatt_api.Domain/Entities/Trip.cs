﻿using knowit_skatt_api.Domain.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace knowit_skatt_api.Domain.Entities
{
    public class Trip : AuditableBaseEntity
    {
        public DateTime Date { get; set; }
        public decimal Fee { get; set; }
        [NotMapped]
        public string Type { get; set; }
        [NotMapped]
        public string RegNr { get; set; }

        public ICollection<VehicleTrip> VehicleTrips { get; set; }
    }
}
