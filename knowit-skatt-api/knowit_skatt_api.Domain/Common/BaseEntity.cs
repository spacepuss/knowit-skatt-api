﻿using System;
using System.Collections.Generic;
using System.Text;

namespace knowit_skatt_api.Domain.Common
{
    public abstract class BaseEntity
    {
        public virtual int Id { get; set; }
    }
}
