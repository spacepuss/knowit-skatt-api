﻿using knowit_skatt_api.Application.Interfaces;
using knowit_skatt_api.Domain.Common;
using knowit_skatt_api.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace knowit_skatt_api.Infrastructure.Persistence.Contexts
{
    public class ApplicationDbContext : DbContext
    {
        private readonly IDateTimeService _dateTime;
        private readonly IAuthenticatedUserService _authenticatedUser;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IDateTimeService dateTime, IAuthenticatedUserService authenticatedUser) : base(options)
        {
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
            _dateTime = dateTime;
            _authenticatedUser = authenticatedUser;
        }
        public DbSet<Product> Products { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<Trip> Trips { get; set; }
        //public DbSet<Fee> Fees { get; set; }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableBaseEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.Created = _dateTime.NowUtc;
                        entry.Entity.CreatedBy = _authenticatedUser.UserId;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModified = _dateTime.NowUtc;
                        entry.Entity.LastModifiedBy = _authenticatedUser.UserId;
                        break;
                }
            }
            return base.SaveChangesAsync(cancellationToken);
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            //All Decimals will have 18,6 Range
            foreach (var property in builder.Model.GetEntityTypes()
            .SelectMany(t => t.GetProperties())
            .Where(p => p.ClrType == typeof(decimal) || p.ClrType == typeof(decimal?)))
            {
                property.SetColumnType("decimal(18,6)");
            }

            builder.Entity<VehicleTrip>()
                .HasKey(vt => new { vt.VehicleId, vt.TripId });
            
            builder.Entity<VehicleTrip>()
                .HasOne(vt => vt.Vehicle)
                .WithMany(v => v.VehicleTrips)
                .HasForeignKey(vt => vt.VehicleId);
            
            builder.Entity<VehicleTrip>()
                .HasOne(vt => vt.Trip)
                .WithMany(t => t.VehicleTrips)
                .HasForeignKey(vt => vt.TripId);
            
            
            //builder.Entity<VehicleFee>()
            //    .HasKey(vf => new { vf.VehicleId, vf.FeeId });
            
            //builder.Entity<VehicleFee>()
            //    .HasOne(vf => vf.Vehicle)
            //    .WithMany(v => v.VehicleFees)
            //    .HasForeignKey(vf => vf.VehicleId);
            
            //builder.Entity<VehicleFee>()
            //    .HasOne(vf => vf.Fee)
            //    .WithMany(f => f.VehicleFees)
            //    .HasForeignKey(vf => vf.FeeId);

            base.OnModelCreating(builder);
        }
    }
}
