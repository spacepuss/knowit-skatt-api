﻿using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Domain.Entities;
using knowit_skatt_api.Infrastructure.Persistence.Contexts;
using knowit_skatt_api.Infrastructure.Persistence.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace knowit_skatt_api.Infrastructure.Persistence.Repositories
{
    public class TripRepositoryAsync : GenericRepositoryAsync<Trip>, ITripRepositoryAsync
    {
        private readonly DbSet<Trip> _trips;
        private readonly ApplicationDbContext _dbContext;

        public TripRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _trips = dbContext.Set<Trip>();
            _dbContext = dbContext;
        }

        public Task<bool> IsUniqueDateAsync(DateTime date)
        {
            return _trips
                .AllAsync(t => t.Date != date);
        }

        public async Task<Trip> AddTripAsync(Trip entity)
        {
            var trip = new Trip { 
                Date = entity.Date,
                Fee = entity.Fee
            };

            var vehicle = new Vehicle
            {
                Type = entity.Type,
                RegNr = entity.RegNr,
                VehicleTrips = new List<VehicleTrip>()
            };

            var vehicleTrip = new VehicleTrip();

            vehicleTrip.Trip = trip;
            vehicleTrip.Vehicle = vehicle;

            vehicle.VehicleTrips.Add(vehicleTrip);

            _dbContext.Vehicles.Add(vehicle);

            _dbContext.Trips.Add(trip);

            //await _trips.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
            return entity;
        }
    }
}
