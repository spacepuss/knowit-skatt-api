﻿using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Domain.Entities;
using knowit_skatt_api.Infrastructure.Persistence.Contexts;
using knowit_skatt_api.Infrastructure.Persistence.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace knowit_skatt_api.Infrastructure.Persistence.Repositories
{
    public class VehicleRepositoryAsync : GenericRepositoryAsync<Vehicle>, IVehicleRepositoryAsync
    {
        private readonly DbSet<Vehicle> _vehicles;

        public VehicleRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _vehicles = dbContext.Set<Vehicle>();
        }

        public Task<bool> IsUniqueRegNrAsync(string regNr)
        {
            return _vehicles
                .AllAsync(p => p.RegNr != regNr);
        }
    }
}
