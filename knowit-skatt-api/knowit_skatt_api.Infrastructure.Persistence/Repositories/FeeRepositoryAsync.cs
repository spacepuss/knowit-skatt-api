﻿using knowit_skatt_api.Application.Interfaces.Repositories;
using knowit_skatt_api.Domain.Entities;
using knowit_skatt_api.Infrastructure.Persistence.Contexts;
using knowit_skatt_api.Infrastructure.Persistence.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace knowit_skatt_api.Infrastructure.Persistence.Repositories
{
    public class FeeRepositoryAsync : GenericRepositoryAsync<Fee>, IFeeRepositoryAsync
    {
        private readonly ApplicationDbContext _dbContext;

        public FeeRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IReadOnlyList<TripProjection>> GetByRegNrAsync(string regNr)
        {
            return await _dbContext.Trips
                .Include(t => t.VehicleTrips)
                .ThenInclude(t => t.Vehicle)
                .Where(t => t.VehicleTrips
                .Any(v => v.Vehicle.RegNr == regNr))
                .Select(v => new TripProjection
                {
                    Date = v.Date,
                    Fee = v.Fee,
                    Type = v.VehicleTrips.Select(vt => vt.Vehicle.Type).FirstOrDefault(),
                    RegNr = v.VehicleTrips.Select(vt => vt.Vehicle.RegNr).FirstOrDefault()
                })
                .ToListAsync();
        }
    }
}
